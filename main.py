#!/usr/bin/env python
# Advanced Warfare
from deck import *
from player import *

if __name__ == "__main__":
    newdeck = CardStack(None, 52)
    newdeck.set_new_deck_order()
    
    # Shuffle 8 times
    for r in range(0,8):
        newdeck.shuffle()

    # Half the deck between two players
    computer = Player(True)
    player = Player()
    computer.deck = newdeck.cuthalf()
    player.deck = newdeck

    totalslots = 2
    handlimit = 3
    print("Dealing " + str(handlimit) + " cards to each player.")
    # Deal 3 cards into the hand of each player
    for i in range(0,handlimit):
        player.hand.addcard(player.deck.topdeal())
        computer.hand.addcard(computer.deck.topdeal())

    play = True
    while play:
        print("Cards left in player deck: " + str(player.deck.numcards))
        print("Cards left in computer deck: " + str(computer.deck.numcards))
        if computer.isdealer:
            for i in range(0,totalslots):
                if computer.hand.numcards > 0:
                    computer.table.addcard(computer.hand.topdeal())
                    print("Computer has selected a card to play in slot " + str(i+1))

        # Display the player's card titles.
        for i in range(0,player.hand.numcards):
            print(str(i+1) + ": " + player.hand.cards[i].title)

        print("Select 2 cards for slots 1 and 2.  Enter 1, 2, or 3.  Enter '0' to quit.")
        slot = player.hand.numcards + 1
        while slot > player.hand.numcards:
            print("Player ncards: " + str(player.hand.numcards))
            slot = int(input('Slot 1: '))
        if slot == 0:
            play = 'q'
            break
        else:
            slot = slot - 1
        player.table.topplace(player.hand.stealat(slot))

        print("")
        # Display the player's card titles.
        for i in range(0,player.hand.numcards):
            print(str(i+1) + ": " + player.hand.cards[i].title)

        slot = player.hand.numcards + 1
        while slot > player.hand.numcards:
            print("Player ncards: " + str(player.hand.numcards))
            slot = int(input('Slot 2: '))
        if slot == 0:
            play = 'q'
            break
        else:
            slot = slot - 1
        player.table.topplace(player.hand.stealat(slot))

        if not computer.isdealer:
            for i in range(0,totalslots):
                if computer.hand.numcards > 0:
                    computer.table.addcard(computer.hand.topdeal())
                    print("Computer has selected a cards to play in slot " + str(i+1))

        print("")
        print("")
        for i in range(0,totalslots):
            print("Resolving battle in slot " + str(i + 1))
            print("Computer played: " + computer.table.cards[i].getshorttext())
            print("You played:      " + player.table.cards[i].getshorttext())
            if player.table.cards[i].value > computer.table.cards[i].value:
                print("YOUR CARD in slot " + str(i + 1) + " WON!")
                player.score = player.score + 1
            else:
                print("Your card in slot " + str(i + 1) + " lost!")
                computer.score = computer.score + 1
            print("")

        print("Scores:")
        print("    Player: " + str(player.score) + "    Computer: " + str(computer.score))
        print("")
        print("")
        # Replenish 2 cards from each player's deck
        if player.deck.numcards > 0:
            player.hand.addcard(player.deck.topdeal())
        if computer.deck.numcards > 0:
            computer.hand.addcard(computer.deck.topdeal())
        if player.deck.numcards > 0:
            player.hand.addcard(player.deck.topdeal())
        if computer.deck.numcards > 0:
            computer.hand.addcard(computer.deck.topdeal())

        # End game if either deck is exhausted.
        if player.deck.numcards == 0 or computer.deck.numcards == 0:
            play = False
