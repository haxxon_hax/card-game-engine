#!/usr/bin/env python
from deck import *
from player import *

if __name__ == "__main__":
    newdeck = CardStack(None, 52)
    newdeck.set_new_deck_order()
    
    newdeck.outfaro()
    newdeck.outfaro()
    newdeck.outfaro()
    newdeck.outfaro()

    print("4 faros")
    newdeck.showcards()

    newdeck.outfaro()
    newdeck.outfaro()
    newdeck.outfaro()
    newdeck.outfaro()

    print("8 faros")
    newdeck.showcards()

    for r in range(0,8):
        newdeck.shuffle()
    players = [Player(True),Player(),Player(),Player()]

    # Deal out 5 cards to 4 players.
    for i in range(0,5):
        for j in range(0,players.__len__()):
            players[j].hand.topplace(newdeck.topdeal())

    # How many cards should we have left from a standard 52-card deck?
    print("Number of cards in deck: " + str(newdeck.numcards))

    for j in range(0,players.__len__()):
        print("Player " + str(j))
        players[j].hand.showcards()

    # Player 4 folds
    discard = CardStack()
    discard.add(players[3].hand.toss())
    print("Number of cards in discard: " + str(discard.numcards))
    print("Number of cards for player 4: " + str(players[3].hand.numcards))

    # Start new war game.
    
