#!/usr/bin/python
""" An engine used for card games. """

import random
from math import floor

CARD_VALUE = [
    None,
    'Ace',
    'Two',
    'Three',
    'Four',
    'Five',
    'Six',
    'Seven',
    'Eight',
    'Nine',
    'Ten',
    'Jack',
    'Queen',
    'King',
    'Joker'
]

CARD_SUIT = [
    'Hearts',
    'Clubs',
    'Diamonds',
    'Spades',
    'Jokers'
]

CARD_SUIT_SHORT = ["H", "C", "D", "S", ""]

class Card(object):
    """A card (from some deck of cards)

    Attributes:
        value: the value of the card (standard deck: 1=Ace, 2=Two, 3=Three, ...)
        suit: the suit of the card (standard deck: 0=Hearts, 1=Clubs, 2=Diamonds, 3=Spades,
            4=Jokers)
        face_up: boolean value of whether the deck is face up or down
        text: text written on the card (if any)
        title: the title of the card (if any)
        image: relative link to an image of the card (gif, jpg, etc.)
    """
    def __init__(self, value, suit, face_up=False, title=None, text=None, image=None):
        self.value = value
        self.suit = suit
        self.face_up = face_up
        self.title = title
        self.text = text
        self.image = image

    def getvalue(self):
        """ Returns the value. """
        return self.value

    def getsuit(self):
        """ Returns the suit. """
        return self.suit

    def getfaceup(self):
        """ Returns the text. """
        return self.face_up

    def gettitle(self):
        """ Returns the title. """
        return self.title

    def gettext(self):
        """ Returns the text. """
        return self.text

    def getimage(self):
        """ Returns the image link. """
        return self.image

    def setvalue(self, value):
        """ Returns the value. """
        self.value = value
        return self.value

    def setsuit(self, suit):
        """ Returns the suit. """
        self.suit = suit
        return self.suit

    def setfaceup(self, face_up):
        """ Sets the face_up boolean flag. """
        self.face_up = face_up
        return self.face_up

    def settext(self, text):
        """ Sets the card text. """
        self.text = text
        return self.text

    def settitle(self, title):
        """ Sets the card title. """
        self.title = title
        return self.title

    def setimage(self, image):
        """ Sets the image link. """
        self.image = image
        return self.image

    def getstandardtext(self):
        """ Returns the text for a standard deck of playing cards. """
        __text = CARD_VALUE[self.value] + " of " + CARD_SUIT[self.suit]

        if self.value == 14:
            return CARD_VALUE[self.value]
        return __text

    def getshorttext(self):
        """ Returns the text for a standard deck of playing cards. """
        __text = str(self.value) + " " + CARD_SUIT_SHORT[self.suit]

        if self.value == 14:
            return CARD_VALUE[self.value]
        return __text


class CardStack(object):
    """A stack of cards

    Attributes:
        cards: An array of cards.
        numcards: The number of cards in cards.
        use_jokers: Boolean that lets us use the jokers
    """

    def __init__(self, cards=None, numcards=52, use_jokers=0):
        if cards is None:
            self.cards = []
        else:
            self.cards = cards
        self.numcards = numcards
        self.use_jokers = use_jokers
        self.card_offset_bell = [0, 0, 1, 1, 2, 2, 3]
        self.card_shuffle_bell = [5, 4, 4, 3, 3, 3, 2, 2, 1, 1]

    def set_new_deck_order(self):
        """ Sets the new deck order of a stack of standard playing cards. """
        self.cards = []
        for suit in range(0, 2):
            for value in range(1, 14):
                self.cards.append(Card(value, suit, False,
                                       Card(value, suit).getstandardtext(),
                                       Card(value, suit).getstandardtext()))
        for suit in range(2, 4):
            for value in reversed(range(1, 14)):
                self.cards.append(Card(value, suit, False,
                                       Card(value, suit).getstandardtext(),
                                       Card(value, suit).getstandardtext()))
        if self.use_jokers > 0:
            for i in range(0, self.use_jokers):
                self.cards.append(Card(14, 4))
        self.numcards = self.cards.__len__()

        return self.cards

    def addcard(self, card):
        """ Adds a card to the bottom of the deck. """
        self.cards.append(card)
        self.numcards = self.cards.__len__()
        return self

    def add(self, cards):
        """ Adds a stack of cards the bottom of the deck. """
        self.cards = self.cards + cards
        self.numcards = self.cards.__len__()
        return self

    def toss(self):
        """ Adds a stack of cards the bottom of the deck. """
        __return = self.cards
        self.cards = CardStack()
        self.numcards = 0
        return __return

    def peek(self, position):
        """ Returns the card at specified position in the deck. """
        return self.cards[position]

    def showcards(self):
        """ Shows all cards of the deck, in order from top to bottom. """
        for card in self.cards:
            print(CARD_VALUE[card.value] + " of " + CARD_SUIT[card.suit])

    def topdeal(self):
        """ Deals off the card from the top of the deck... into oblivion. """
        if self.numcards > 0:
            __return = self.cards[0]
        else:
            __return = None
        self.cards.pop(0)
        self.numcards = self.cards.__len__()
        return __return

    def pull(self,n=1):
        """ Pulls off n cards from the top of the deck... into oblivion. """
        __return = self.cards
        if n < self.numcards:
            __return = self.cards[:n]
            self.cards = self.cards[n:]
            self.numcards = self.cards.__len__()
        return __return

    def stealat(self,n):
        """ Steals card at n position from the deck. """
        if n < self.numcards:
            __return = self.cards[n]
            self.cards.pop(n)
            self.numcards = self.cards.__len__()
        else:
            return None
        return __return

    def bottomdeal(self):
        """ Deals off the card from the bottom of the deck... into oblivion. """
        __return = self.cards[self.numcards-1]
        self.cards.pop(self.numcards-1)
        self.numcards = self.cards.__len__()
        return __return

    def seconddeal(self):
        """ Deals off the card 2nd from the top of the deck... into oblivion. """
        __return = self.cards[1]
        self.cards.pop(1)
        self.numcards = self.cards.__len__()
        return __return

    def topplace(self, card):
        """ Places a new card on the top of the deck. """
        self.cards = [card] + self.cards
        self.numcards == self.cards.__len__()
        return self.cards

    def cutat(self,n):
        """ Cuts at specified position n. """
        newpile = CardStack()
        if n < self.numcards:
            newpile.cardlist = self.cards[:n]
            newpile.num_cards = newpile.cards.__len__()
            self.cards = self.cards[n:]
            self.numcards = self.cards.__len__()
        else:
            newpile.num_cards = 0
        return newpile

    def cuthigh(self):
        """ Cuts the deck in the upper portion. """
        randoffset = random.randint(0, self.card_offset_bell.__len__() - 1)
        offset = self.card_offset_bell[random.randint(0, self.card_offset_bell.__len__() - 1)]
        addorsub = random.randint(0, 1)
        if addorsub == 0:
            cutat = int(floor(self.numcards/4) + offset)
        else:
            cutat = int(floor(self.numcards/4) - offset)
        if cutat < 0:
            cutat = 1
        if cutat > self.numcards:
            cutat = self.numcards - 1
        cutportion = CardStack(self.cards[:cutat], self.cards[:cutat].__len__())
        self.cards = self.cards[cutat:]
        self.numcards = self.cards.__len__()
        return cutportion

    def cutlow(self):
        """ Cuts the deck in the lower portion. """
        randoffset = random.randint(0, self.card_offset_bell.__len__() - 1)
        offset = self.card_offset_bell[random.randint(0, self.card_offset_bell.__len__() - 1)]
        addorsub = random.randint(0, 1)
        if addorsub == 0:
            cutat = int(floor(self.numcards*3/4) + offset)
        else:
            cutat = int(floor(self.numcards*3/4) - offset)
        if cutat < 0:
            cutat = 1
        if cutat > self.numcards:
            cutat = self.numcards - 1
        cutportion = CardStack(self.cards[:cutat], self.cards[:cutat].__len__())
        self.cards = self.cards[cutat:]
        self.numcards = self.cards.__len__()
        return cutportion

    def cutmiddle(self):
        """ Cuts the deck in the middle. """
        randoffset = random.randint(0, self.card_offset_bell.__len__() - 1)
        offset = self.card_offset_bell[random.randint(0, self.card_offset_bell.__len__() - 1)]
        addorsub = random.randint(0, 1)
        if addorsub == 0:
            cutat = int(floor(self.numcards/2) + offset)
        else:
            cutat = int(floor(self.numcards/2) - offset)
        if cutat < 0:
            cutat = 1
        if cutat > self.numcards:
            cutat = self.numcards - 1
        cutportion = CardStack(self.cards[:cutat], self.cards[:cutat].__len__())
        self.cards = self.cards[cutat:]
        self.numcards = self.cards.__len__()
        return cutportion

    def cuthalf(self):
        """ Cuts the deck exactly in the middle. """
        cutat = int(floor(self.numcards/2))
        if cutat < 0:
            cutat = 1
        if cutat > self.numcards:
            cutat = self.numcards - 1
        cutportion = CardStack(self.cards[:cutat], self.cards[:cutat].__len__())
        self.cards = self.cards[cutat:]
        self.numcards = self.cards.__len__()
        return cutportion

    def reverse(self):
        """ Reverses the order of a stack of cards. """
        newpile = CardStack()
        for card in self.cards:
            newpile.topplace(card)
        self.cards = newpile.cards
        return self

    def shuffle(self):
        """ Simulates a real-world shuffle. """
        newpile = CardStack()
        halfdecks = [self.cutmiddle()]
        halfdecks.append(self)
        halfdecks[0].reverse()
        halfdecks[1].reverse()
        done = False
        dropcount = [0, 0]
        dropmax = [halfdecks[0].numcards, halfdecks[1].numcards]
        deckoneortwo = random.randint(0,1)
        while done is False:
            dropnum = self.card_offset_bell[random.randint(0, self.card_offset_bell.__len__() - 1)]
            for i in range(0,dropnum):
                if dropcount[0] >= dropmax[0] and dropcount[1] >= dropmax[1]:
                    done = True
                else:
                    if dropcount[deckoneortwo] < dropmax[deckoneortwo]:
                        newpile.topplace(halfdecks[deckoneortwo].topdeal())
                        dropcount[deckoneortwo] = dropcount[deckoneortwo] + 1
            deckoneortwo = (deckoneortwo + 1) % 2

        newpile.numcards = newpile.cards.__len__()

        self.cards = newpile.cards
        self.numcards = newpile.cards.__len__()
        return newpile

    def overhandshuffle(self):
        """ Simulates a real-world overhand shuffle. """
        newpile = CardStack()
        dropcount = 0
        done = False
        maxdrop = self.numcards
        while done is False:
            dropnum = self.card_offset_bell[random.randint(0, self.card_offset_bell.__len__() - 1)]
            if dropcount >= maxdrop:
                done = True
            if dropnum > self.cards.__len__():
                dropnum = self.cards.__len__()
            newpile.cards = self.cards[:dropnum] + newpile.cards
            newpile.numcards = newpile.cards.__len__()
            self.cards = self.cards[dropnum:]
            self.numcards = self.cards.__len__()
            dropcount = dropcount + dropnum
        self.cards = newpile.cards
        self.numcards = newpile.cards.__len__()
        return self.cards

    def outfaro(self):
        """ Simulates an out-faro shuffle. """
        newpile = self.cuthalf()
        piles = [newpile, self]
        newpile = CardStack()
        pullfrom = 1
        pull = True
        while pull == True:
            if piles[pullfrom].numcards > 0:
                newpile.topplace(piles[pullfrom].bottomdeal())
            pullfrom = (pullfrom + 1) % 2
            if piles[0].numcards == 0 and piles[1].numcards == 0:
                pull = False
        newpile.numcards = newpile.cards.__len__()
        self.cards = newpile.cards
        self.numcards = newpile.numcards
        return newpile

    def infaro(self):
        """ Simulates an in-faro shuffle. """
        newpile = self.cuthalf()
        piles = [newpile, self]
        newpile = CardStack()
        pullfrom = 0
        pull = True
        while pull == True:
            if piles[pullfrom].numcards > 0:
                newpile.topplace(piles[pullfrom].bottomdeal())
            pullfrom = (pullfrom + 1) % 2
            if piles[0].numcards == 0 and piles[1].numcards == 0:
                pull = False
        newpile.numcards = newpile.cards.__len__()
        self.cards = newpile.cards
        self.numcards = newpile.numcards
        return newpile

