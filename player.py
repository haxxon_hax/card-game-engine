#!/usr/bin/env python
from deck import *

class Player(object):
    def __init__(self,isdealer=False):
        self.hand = CardStack()
        self.deck = CardStack()
        self.table = CardStack()
        self.playarea = CardStack()
        self.stacks = []
        self.isdealer = isdealer
        self.score = 0


